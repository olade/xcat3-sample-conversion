import sys
import cca

from jarray import zeros

from java.lang import System, Thread
from java.lang import String, Object

# get the absolute location for XCAT
xcatHome = System.getProperty("XCAT_HOME")
if xcatHome == None:
    print "Jython script: Property XCAT_HOME not set"
    System.exit(0)

# use the TypeMap for component instantiation
# create the TypeMap for the provider component
providerMap = cca.createTypeMap()
providerMap.putString("className", "com.computome.samples.xcat.conversion.ProvidesConversionComponent")
providerMap.putString("execDir", xcatHome + "/src/java/scripts")
providerMap.putString("execName", "ContainerLauncher.sh")
providerMap.putString("sshHome", "/usr/local/bin/ssh")

if len(sys.argv) >= 1:
  if sys.argv[0] == "notify":
    providerMap.putString("topic", System.getProperty("notification.topic"))
    providerMap.putString("consumer", System.getProperty("notification.consumer.url"))
    if System.getProperty("notification.producer.url") != "":
       providerMap.putString("producer", System.getProperty("notification.producer.url"))

# create the TypeMap for the user component
userMap = cca.createTypeMap()
userMap.putString("className", "com.computome.samples.xcat.conversion.UsesConversionComponent")
userMap.putString("execDir", xcatHome + "/src/java/scripts")
userMap.putString("execName", "ContainerLauncher.sh")
userMap.putString("sshHome", "/usr/local/bin/ssh")

# create component wrappers
provides = cca.createComponentWrapper("provider", providerMap)
uses = cca.createComponentWrapper("user", userMap)

# assign a machine name
cca.setMachineName(uses, "rainier.extreme.indiana.edu")
cca.setMachineName(provides, "rainier.extreme.indiana.edu")

# set a creation mechanism to in-process
cca.setCreationMechanism(uses, "local")
cca.setCreationMechanism(provides, "local")

# create live instances
cca.createInstance(uses)
print "Jython script: Created the uses component"
cca.createInstance(provides)
print "Jython script: Created the provides component"

# connect their ports
cca.connectPorts(uses, "convertUsesPort", provides, "convertProvidesPort")

# start the components
# For goPort
portClassName = "intf.ports.XCATGoPort"
portType = "http://www.extreme.indiana.edu/xcat/ports/go"
providesPortName = "providesGoPort"
methodName = "go"
methodParams = zeros(0, Object)

print "Jython script: Invoking method on uses's go port"
cca.invokeMethodOnComponent(uses,
          portClassName,
          portType,
          providesPortName,
          methodName,
          methodParams)

print "Waiting for 5 seconds to destroy components"
Thread.sleep(5000)
cca.destroy(uses)
cca.destroy(provides)

print "Done"
