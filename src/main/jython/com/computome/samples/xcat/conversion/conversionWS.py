import sys
import cca

from jarray import zeros

from java.lang import System, Thread
from java.lang import String, Object

# get the absolute location for XCAT
xcatHome = System.getProperty("XCAT_HOME")
if xcatHome == None:
    print "Jython script: Property XCAT_HOME not set"
    System.exit(0)

if len(sys.argv) != 1:
    print "Usage: run.sh script ./src/jython/conversionWS.py <WS_URL>"
    System.exit(0)
endPointLocation = sys.argv[0]

# create the TypeMap for the user component
userMap = cca.createTypeMap()
userMap.putString("className",
		  "samples.conversion.UsesConversionComponent")
userMap.putString("execDir",
		  xcatHome + "/src/java/scripts")
userMap.putString("execName",
		  "ContainerLauncher.sh")
userMap.putString("sshHome",
		  "/usr/local/bin/ssh")
userMap.putBool("useWSPort", 1)

# create component wrappers
uses = cca.createComponentWrapper("user", userMap)

# assign a machine name
cca.setMachineName(uses, "rainier.extreme.indiana.edu")

# set a creation mechanism to in-process
cca.setCreationMechanism(uses, "local")

# create live instances
cca.createInstance(uses)
print "Jython script: Created the uses component"

# connect their ports
cca.connectPortToWS(uses, "convertWSPort", endPointLocation)

# start the components
# For goPort
portClassName = "intf.ports.XCATGoPort"
portType = "http://www.extreme.indiana.edu/xcat/ports/go"
providesPortName = "providesGoPort"
methodName = "go"
methodParams = zeros(0, Object)

print "Jython script: Invoking method on uses's go port"
cca.invokeMethodOnComponent(uses, 
			    portClassName, 
			    portType, 
			    providesPortName, 
			    methodName, 
			    methodParams)

print "Waiting for 5 seconds to destroy components"
Thread.sleep(5000)
cca.disconnectPortFromWS(uses, "convertWSPort")
cca.destroy(uses)

