# The Jython API to XCAT
# @author : Sriram Krishnan[srikrish@extreme.indiana.edu]

import sys

# Import XCAT's UI classes
from xcat.jython import CompositionTool
from xcat.util import HandleResolver

# Create a composition tool. Declare it as global
# so that all the functions can access the component.
global compositionTool
compositionTool = CompositionTool()

# This method expects an instanceName and the XML string (or TypeMap)
# describing the component, and creates a component wrapper 
# for it. A live instance can be created by invoking
# the createInstance() method
def createComponentWrapper(instanceName, componentInfo):

    componentWrapper = compositionTool.createComponentWrapper(instanceName, componentInfo)
    return componentWrapper

# setMachineName(componentWrapper, machineName) specifies
# the machine on which we would like to instantiate
# the component.
def setMachineName(componentWrapper, machineName):

    componentWrapper.setMachineName(machineName)
    return 


# setCreationMechanism(componentWrapper, creationMechanism)
# specifies the creation mechanism to be used while creating
# the component instance.
def setCreationMechanism(componentWrapper, creationMechanism):

    componentWrapper.setCreationMechanism(creationMechanism)
    return


# createInstance takes in a handle to the component
# wrapper that we want to create and then creates a
# live instance of this component.
def createInstance(componentWrapper):

    compositionTool.instantiate(componentWrapper)
    return

# createInstance takes in a handle to the component
# wrapper that we want to create and then creates a
# live instance of this component.
def createInstanceWithTimeOut(componentWrapper, timeout):

    compositionTool.instantiate(componentWrapper, timeout)
    return

# connectPorts connects the uses port 'usesPort' of
# the component 'usesPortComponent' to the provides port
# 'providesPort' of the component 'providesPortComponent'.
def connectPorts(usesPortComponent, 
		 usesPortName, 
		 providesPortComponent, 
		 providesPortName):
    
    connectionID = compositionTool.connect(usesPortComponent, 
					   usesPortName, 
					   providesPortComponent, 
					   providesPortName)
    return connectionID

# connectPortToWS connects a WS port to a Web service specified
# by its endPointLocation
def connectPortToWS(wsPortComponent, 
		    wsPortName, 
		    endPointLocation):
    
    compositionTool.connectToWS(wsPortComponent, 
				wsPortName, 
				endPointLocation)
    return

# disconnectPorts disconnects connection represented by the 
# connectionID, using the timeout value
def disconnectPorts(connectionID,
		    timeout):
    
    compositionTool.disconnect(connectionID,
			       timeout)
    return

# disconnectPortFromWS disconnects connection between a WS Port
# and a Web service
def disconnectPortFromWS(wsPortComponent,
			 wsPortName):
    
    compositionTool.disconnectFromWS(wsPortComponent,
				     wsPortName)
    return

# method that takes in the component specified by the 
# componentWrapper, the fully qualified name of the port, 
# its type (which is a uri), the name under which the provides 
# port is registered, the method name, and the parameters, and 
# invokes the method on it. All parameters are strings, except 
# the methodParams, which is an array of java objects.
def invokeMethodOnComponent(componentWrapper, 
			    portClassName, 
			    portType, 
			    providesPortName, 
			    methodName, 
			    methodParams):
    
    retVal = compositionTool.executeMethod(componentWrapper, 
					   portClassName, 
					   portType, 
					   providesPortName, 
					   methodName, 
					   methodParams)
    return retVal

# method that takes in the component specified by the 
# componentWrapper, the fully qualified name of the port, 
# its type (which is a uri), the name under which the provides 
# port is registered, the method name, and the parameters, the
# types for the parameters and invokes the method on it. All 
# parameters are strings, except the methodParams, which is an 
# array of java objects, and methodParamTypes, which is an array
# of java Class objects
def invokeMethodOnComponentWithTypes(componentWrapper, 
				     portClassName, 
				     portType, 
				     providesPortName, 
				     methodName, 
				     methodParams,
				     methodParamTypes):
    
    retVal = compositionTool.executeMethod(componentWrapper, 
					   portClassName, 
					   portType, 
					   providesPortName, 
					   methodName, 
					   methodParams,
					   methodParamTypes)
    return retVal

# returns an instance of the gov.cca.TypeMap object
def createTypeMap():
    return compositionTool.createTypeMap()

# destroys this component instance
def destroy(componentWrapper):
    compositionTool.destroy(componentWrapper)
    return

# sets the location of the HandleResolver
def setHandleResolverURL(handleResolverURL):
    HandleResolver.setHandleResolverURL(handleResolverURL)
    return
