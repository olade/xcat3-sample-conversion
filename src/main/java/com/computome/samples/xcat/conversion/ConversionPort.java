package com.computome.samples.xcat.conversion;

import intf.ports.XCATPort;

/**
 * The interface definition for the Conversion Port.
 */
public interface ConversionPort extends XCATPort {

  /**
   * Method to convert a value in celsius to fahrenheit.
   */
  public float centigradeToFahrenheit(float celcius);

}
