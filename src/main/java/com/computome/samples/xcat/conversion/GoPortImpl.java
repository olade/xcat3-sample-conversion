/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.5 $ $Author: srikrish $ $Date: 2004/03/05 22:35:18 $ (GMT)
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package com.computome.samples.xcat.conversion;

import intf.ports.XCATGoPort;
import soaprmi.util.logging.Logger;
import xcat.ports.BasicPortImpl;

/**
 * The implementation of the XCATGoPort
 */
public class GoPortImpl extends BasicPortImpl
  implements XCATGoPort {

  private static Logger logger = Logger.getLogger();
  private UsesConversionComponent usesComp;

  /**
   * Default constructor, so that this port can be instantiated via reflection
   */
  public GoPortImpl() throws Exception {
    super();
  }

  /**
   * Constructor
   * @param ucc reference to the component which contains this port, so
   * this port can make callbacks.
   */
  public GoPortImpl(UsesConversionComponent ucc) throws Exception {
    super();
    usesComp = ucc;
  }

  /**
   * Execute some encapsulated functionality on the component.
   * Return 0 if ok, -1 if internal error but component may be
   * used further, and -2 if error so severe that component cannot
   * be further used safely.
   */
  public int go() {
    // return -2 if the UsesConversionComponent is null
    if (usesComp == null)
      return -2;

    // run the startExecuting method of component in a separate thread
    new Thread() {
	public void run() {
	  logger.finest("GoPortImpl: Starting execution of UsesConversionComponent");
	  usesComp.startExecuting();
	}
      }.start();

    // return 0 on successful completion
    return 0;
  }

  /**
   * Non remote method used to set the reference to the component using the port
   * @param usesComp_ the reference the component that contains this port
   */
  public void setUsesConversionComponent(UsesConversionComponent usesComp_) {
    usesComp = usesComp_;
  }
}
