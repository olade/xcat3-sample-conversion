/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.6 $ $Author: yihuan $ $Date: 2004/10/08 05:59:37 $ (GMT)
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package com.computome.samples.xcat.conversion;

import xcat.ports.BasicPortImpl;
import soaprmi.util.logging.Logger;
import soaprmi.soaprpc.SoapServices;
import soaprmi.server.UnicastRemoteObject;
import wsnt.WsntClientAPI;
import gov.cca.*;

/**
 * This is the implementation of the ConversionPort
 * @version $Revision: 1.6 $  $Author: yihuan $ $Date: 2004/10/08 05:59:37 $ (GMT)
 * @author Sriram Krishnan [mailto: srikrish@extreme.indiana.edu]
 * @author Yi Huang [mailto: yihuan@cs.indiana.edu]
 */

public class ConversionPortImpl
    extends BasicPortImpl
    implements ConversionPort {

  private static Logger logger = Logger.getLogger();
  private TypeMap props = null;
  private String topic = null;
  private String producer = null;
  private String consumer = null;

  /**
   * Default constructor.
   */
  public ConversionPortImpl() throws Exception {
    super();
  }

  public ConversionPortImpl(TypeMap props) throws Exception {
    super();
    this.props = props;
  }

  /**
   * Method to convert a value in celcius to centigrade
   */
  public float centigradeToFahrenheit(float celcius) {
    logger.finest("ConversionPortImpl: Received celcius value: " + celcius);
    float fahr = ( (9f / 5f) * celcius) + 32;
    logger.finest("ConversionPortImpl: Returning fahrenheit value: " + fahr);

    if (props != null) {
      try {
        topic = props.getString("topic", null); //topic
        producer = props.getString("producer",
                                   null); //Producer URL
        consumer = props.getString("consumer",
                                   null); //Consumer (Broker) URL
      }
      catch (TypeMismatchException ex) {
        logger.severe(
            "Exception while trying to retrieve property: providePort");
      }
    }

    if (topic != null && consumer != null) {
      WsntClientAPI publisher = new WsntClientAPI();
      System.out.println("***********Sending Event...");
      String message = "Result: celcius value " + celcius + " equals " + fahr +
          " fahrenheit.";
      publisher.publish(consumer, producer, topic, message);
    }

    return fahr;
  }

  /**
   * A main method in case the service has to be started from the command line
   */
  public static void main(String[] args) throws Exception {

    // start the service on an available port
    ConversionPort cPort = new ConversionPortImpl();
    UnicastRemoteObject.exportObject(cPort,
                                     new Class[] {ConversionPort.class});
    logger.finest("Conversion service up and ready to serve requests at: " +
                  SoapServices.getDefault().getStartpointLocation(cPort));
  }
}
