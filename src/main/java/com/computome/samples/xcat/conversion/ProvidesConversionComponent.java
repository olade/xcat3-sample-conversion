/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.8 $ $Author: yihuan $ $Date: 2004/10/08 05:59:37 $ (GMT)
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package com.computome.samples.xcat.conversion;

import gov.cca.Services;
import gov.cca.TypeMap;

import intf.mobile.ccacore.MobileComponent;

import soaprmi.util.logging.Logger;

import xcat.exceptions.UnexpectedException;
import intf.ccacore.XCATServices;

public class ProvidesConversionComponent
  implements MobileComponent {

  private static Logger logger = Logger.getLogger();

  // instance of the services object - every CCA component needs one
  private Services providesConvertCore;

  // instance of the convert port
  private ConversionPortImpl convertImpl;

  /**
   * Default empty constructor: Required
   */
  public ProvidesConversionComponent() {
    logger.finest("called");
  }

  /**
   * The only method defined by the Component interface
   * @param cc the services object to initialize the component with
   */
  public void setServices(Services cc)
    throws gov.cca.CCAException {

    providesConvertCore = cc;

    // Adding a provides port
    logger.finest("added convertProvidesPort");
    TypeMap pMap = providesConvertCore.createTypeMap();
    pMap.putString("portClass",
		   ConversionPort.class.getName());
    TypeMap props = ((XCATServices) providesConvertCore).getProperties();

    try {
      convertImpl = new ConversionPortImpl(props);
    } catch (Exception e) {
      logger.severe("can't instantiate ConversionPort", e);
      throw new UnexpectedException("can't instantiate ConversionPort", e);
    }

    providesConvertCore.addProvidesPort(convertImpl,
					"convertProvidesPort",
					"http://www.extreme.indiana.edu/xcat/ports/convert",
					pMap);
  }

  //----------------------------------------------------//
  //  List of methods to load and store component state //
  //----------------------------------------------------//

  /**
   * Returns the state of the component
   */
  public String generateComponentState()
    throws gov.cca.CCAException {
    // component is stateless
    return null;
  }

  /**
   * Sets the state of the component from its stringified form
   * @param services the services object that the component should use
   * @param componentState the state that the component loads
   */
  public void setComponentState(Services services,
				String componentState)
    throws gov.cca.CCAException {

    // load the services object
    providesConvertCore = services;

    // ignores the state since the component is stateless
  }

  //--------------------------------------------------//
  //   List of methods to stop and resume execution   //
  //--------------------------------------------------//

  /**
   * Resumes execution of a component that has been restarted
   */
  public void resumeExecution()
    throws gov.cca.CCAException {
    // ignore, since the component has no threads of its own
  }
}
