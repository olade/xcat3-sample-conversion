package com.computome.samples.xcat.conversion;

import gov.cca.Component;
import gov.cca.Services;
import gov.cca.TypeMap;

import soaprmi.util.logging.Logger;

import xcat.exceptions.UnexpectedException;

/**
 * The implementation of the UsesConversionComponent. Every XCAT3 Component implements the
 * gov.cca.Component interface.
 */
public class UsesConversionComponent implements Component {

  private static Logger logger = Logger.getLogger();

  // Every component contains an instance of gov.cca.Services
  private Services usesConvertCore;

  // Instance of a GoPort for this component to bootstrap execution
  private GoPortImpl goPort;

  /**
   * Default empty constructor: Required for instantiation
   */
  public UsesConversionComponent() {
    logger.finest("called");
  }

  /**
   * The only method defined by the Component interface
   *
   * @param cc the services object to initialize the component with
   */
  public void setServices(Services cc) throws gov.cca.CCAException {
    logger.finest("called");

    // set the Services object to the one passed
    usesConvertCore = cc;

    // Addition of a uses port requires a gov.cca.TypeMap object.
    // The TypeMap object needs to contain the portClass entry with the
    // fully qualified class name of the interface defining the port.
    TypeMap uMap = usesConvertCore.createTypeMap();
    uMap.putString("portClass", ConversionPort.class.getName());

    // param[0] : name of port to register
    // param[1] : namespace for the port
    // THIS HAS TO BE THE SAME AS THAT OF MATCHING PROVIDES PORT
    // param[2] : typeMap object for the port as defined above
    usesConvertCore.registerUsesPort("convertUsesPort",
        "http://www.extreme.indiana.edu/xcat/ports/convert", uMap);

    // create an instance of the GoPort
    try {
      goPort = new GoPortImpl(this);
    } catch (Exception e) {
      logger.severe("can't instantiate GoPort", e);
      throw new UnexpectedException("can't instantiate GoPort", e);
    }

    // Addition of a provides port requires a gov.cca.TypeMap object.
    // The TypeMap object needs to contain the portClass entry with the
    // fully qualified class name of the interface defining the port.
    TypeMap pMap = usesConvertCore.createTypeMap();
    pMap.putString("portClass", intf.ports.XCATGoPort.class.getName());

    // param[0] : the instance of the port to add
    // param[1] : name of port to register
    // param[2] : namespace for the port
    // param[3] : typeMap object for the port as defined above
    usesConvertCore.addProvidesPort(goPort, "providesGoPort",
        "http://www.extreme.indiana.edu/xcat/ports/go", pMap);
  }

  /**
   * This method represents whatever computation the component needs to do. This method is called by
   * the GoPort's go method
   */
  public void startExecuting() {
    try {
      // Get a reference to a usesConvert port to invoke remote methods
      ConversionPort usesConvert = (ConversionPort) usesConvertCore.getPort("convertUsesPort");

      // Make a few calls
      logger.finest("UsesConversionComponent: Celcius : 0, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(0f));
      logger.finest("UsesConversionComponent: Celcius : 20, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(20f));
      logger.finest("UsesConversionComponent: Celcius : 40, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(40f));
      logger.finest("UsesConversionComponent: Celcius : 60, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(60f));
      logger.finest("UsesConversionComponent: Celcius : 80, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(80f));
      logger.finest("UsesConversionComponent: Celcius : 100, Fahrenheit: "
          + usesConvert.centigradeToFahrenheit(100f));

      // Release the port when you are done using
      // If it is not released, another getPort with the
      // same arguments will block
      usesConvertCore.releasePort("convertUsesPort");
    } catch (Exception e) {
      logger.severe("Exception in startExecuting", e);
    }
  }

}
