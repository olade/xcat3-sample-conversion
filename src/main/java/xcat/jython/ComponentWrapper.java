/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.14 $ $Author: srikrish $ $Date: 2004/09/07 23:22:56 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * Copyright (c) 2002 Extreme! Lab, Indiana University. All rights reserved.
 *
 * This software is open source. See the bottom of this file for the licence.
 *
 * $Id: ComponentWrapper.java,v 1.14 2004/09/07 23:22:56 srikrish Exp $
 * @version $Revision: 1.14 $
 * http://www.extreme.indiana.edu/xcat
 */

package xcat.jython;

import java.io.Reader;
import java.io.StringReader;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import soaprmi.util.logging.Logger;

import org.exolab.castor.xml.Unmarshaller;

import intf.ccacore.XCATComponentID;
import intf.services.XCATBuilderService;

import xcat.types.TypeMapImpl;
import xcat.schema.ComponentStaticInformation;
import xcat.schema.ComponentInformation;
import xcat.schema.ExecutionEnv;
import xcat.schema.NameValuePair;
import xcat.schema.PortList;
import xcat.exceptions.UnexpectedException;

import gov.cca.CCAException;
import gov.cca.ComponentID;
import gov.cca.TypeMap;

/**
 * The ComponentWrapper acts as a bridge between the XCAT Jython API
 * and the Java Builder Service
 *
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 * @version $Revision: 1.14 $  $Author: srikrish $ $Date: 2004/09/07 23:22:56 $ (GMT)
 */
public class ComponentWrapper {

  private static Logger logger = Logger.getLogger();

  // ComponentID for the component
  private ComponentID componentID;

  // Execution environment for the component
  private TypeMap componentEnv;

  // Castor generated classes from XSD
  private String uniqueID;
  private String name;
  private String[] authorList;
  private String[] compAuthorList;
  private PortList portList;
  private ExecutionEnv[] executionEnvList;
  private String execHost;
  private String creationProto;

  // Other information about this instance
  private String instanceName;
  private String className;
  private String componentXML;

  // reference to a BuilderService
  private XCATBuilderService builderService;

  /**
   * @param name Name of this component instance
   * @param componentEnv TypeMap containing execution environment
   * @param builderService reference to a Builder to use for instantiation
   */
  public ComponentWrapper(String name, 
			  TypeMap componentEnv,
			  XCATBuilderService builderService) {
    
    logger.finest("called with parameters: " + 
		  " name = " + name +
		  " componentEnv = " + componentEnv);

    this.componentEnv = componentEnv;
    this.instanceName = name;
    this.builderService = builderService;
  }

  /**
   * @param name Name of this component instance
   * @param componentXML XML description of the Component
   * @param builderService reference to a Builder to use for instantiation
   */
  public ComponentWrapper(String name, 
			  String componentXML,
			  XCATBuilderService builderService) 
    throws CCAException {
    
    logger.finest("called with parameters: " +
		  " name = " + name);

    this.instanceName = name;
    this.builderService = builderService;
    this.componentXML = componentXML;

    // set the list of valid installations in executionEnvList
    try {
      // parse the xml description of the component
      StringReader readerCompStaticInfo = new StringReader(componentXML);
      ComponentStaticInformation compStaticInfo  =
        (ComponentStaticInformation)Unmarshaller.unmarshal
        (ComponentStaticInformation.class, readerCompStaticInfo);
      ComponentInformation compInfo =
        compStaticInfo.getComponentInformation();

      // get some information about the component
      uniqueID = compInfo.getUniqueID();
      name = compInfo.getName();
      authorList = compInfo.getAuthor();
      compAuthorList = compInfo.getComponentAuthor();
      portList = compInfo.getPortList();

      // get the list of the valid execution environments
      executionEnvList = compStaticInfo.getExecutionEnv();
    } catch (Exception e) {
      logger.severe("Exception thrown while parsing XML", e);
      throw new UnexpectedException("Can't create ComponentWrapper", e);
    }
  }

  /**
   * Sets the machine name to launch the component
   * @param machineName The target machine name
   */
  public void setMachineName(String machineName) {
    
    logger.finest("called with machineName: " + machineName);
    execHost = machineName;
  }

  /**
   * Sets the creation mechanism for the component launch
   * @param creationMechanism The creation mechanism to be used
   */
  public void setCreationMechanism(String creationMechanism) {

    logger.finest("called with creationMechanism: " + creationMechanism);
    creationProto = creationMechanism;
  }

  /**
   * Creates a live instance of the component
   * @param timeout timeout in milliseconds
   */
  public void createInstance(long timeout) 
    throws CCAException {

    logger.finest("called");

    // check if the creationProto is set
    if (creationProto == null) {
      String message = "Creation mechanism not set!!";
      logger.severe(message);
      throw new UnexpectedException(message);
    }

    // check if execHost is set
    if (execHost == null) {
      String message = "Exec host not set!!";
      logger.severe(message);
      throw new UnexpectedException(message);
    }

    // if the TypeMap is non-null, it has been init before
    // this can happen only if ComponentWrapper has been created by
    // passing in the TypeMap via constructor
    if (componentEnv != null) {
      componentEnv.putString("execHost", execHost);
      componentEnv.putString("creationProto", creationProto);
    } else {
      
      // create a new TypeMap and fill in the values
      componentEnv = new TypeMapImpl();
      
      componentEnv.putString("execHost", execHost);
      componentEnv.putString("creationProto", creationProto);
      
      // check if a valid installation exists for this host/proto
      boolean found = false;
      for (int k = 0; k < executionEnvList.length; k++) {
        ExecutionEnv executionEnv = executionEnvList[k];
        String[] hostName = executionEnv.getHostName();
        String[] creationMech = executionEnv.getCreationProto();

	// check if there is a match for the hostName
	int h;
	for (h = 0; h < hostName.length; h++) {
	  if (hostName[h].equals(execHost)) {
	    break;
	  }
	}
	// if h equals hostName.length, then skip
	if (h == hostName.length)
	  continue;

        // check if there is a match for creationProto
        int m;
        for (m = 0; m < creationMech.length; m++) {
          if (creationMech[m].equals(creationProto)) {
            break;
          }
        }
        // if h equals creationMech.length, then skip
        if (m == creationMech.length)
          continue;

        // if we get here, this is a valid installation for this
	// creationProto/execHost pair
        NameValuePair[] nameValuePairList =
          executionEnv.getNameValuePair();
        for (int l = 0; l < nameValuePairList.length; l++) {
          NameValuePair nameValuePair = nameValuePairList[l];
          String nameOfVariable = nameValuePair.getName();
          String value = nameValuePair.getValue();
          componentEnv.putString(nameOfVariable, value);
        }

        // found an installation, break here
        found = true;
        break;
      }

      // if no valid installation found, throw CCAException
      if (!found) {
        String message = new String ("No valid installation for : " +
                                     execHost + " , " + creationProto);
        logger.severe(message);
        throw new UnexpectedException(message);
      }
    }

    // get the name of the class if it exists
    className = componentEnv.getString("className", 
				       "None");
    
    // add the timeout as a parameter
    componentEnv.putLong("timeout", timeout);

    // if the component has been instantiated before, use the same GSH
    // notify that this is a migration
    if (componentID != null) {
      componentEnv.putString("componentHandle", 
			     ((XCATComponentID) componentID).getGSH());
      componentEnv.putBool("isMigrated", true);
    }
    if (className.equals("None"))
      throw new UnexpectedException("Property className for component not defined");

    // invoke the Builder Service to create a new instance
    componentID = builderService.createInstance(instanceName,
						className,
						componentEnv);
  }

  /**
   * Creates a live instance using default timeout
   */
  public void createInstance()
    throws CCAException {
    logger.finest("called");
    createInstance(XCATBuilderService.DEFAULT_TIMEOUT);
  }

  /**
   * Returns the ComponentID associated with this ComponentWrapper
   */
  public ComponentID getComponentID() {
    logger.finest("called");
    return componentID;
  }

  /**
   * Returns the instance name of the Component
   */
  public String getInstanceName() {
    logger.finest("called");
    return instanceName;
  }

  /**
   * Returns the machineName where this component is to be launched
   */
  public String getMachineName() {
    logger.finest("called");
    return execHost;
  }

  /**
   * Returns the creation protocol used
   */
  public String getCreationProto() {
    logger.finest("called");
    return creationProto;
  }

  /**
   * Returns the XML descriptor for this component
   */
  public String getComponentXML() {
    logger.finest("called");
    return componentXML;
  }
}
