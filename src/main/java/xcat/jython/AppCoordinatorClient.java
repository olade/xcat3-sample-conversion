/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.23 $ $Author: srikrish $ $Date: 2004/09/09 06:55:33 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * Copyright (c) 2002 Extreme! Lab, Indiana University. All rights reserved.
 *
 * This software is open source. See the bottom of this file for the licence.
 *
 * $Id: AppCoordinatorClient.java,v 1.23 2004/09/09 06:55:33 srikrish Exp $
 * @version $Revision: 1.23 $
 * http://www.extreme.indiana.edu/xcat
 */

package xcat.jython;

import intf.ccacore.XCATComponentID;
import intf.mobile.coordinator.ComponentInfo;
import intf.mobile.coordinator.AppCoordinator;

import xcat.util.HandleResolver;
import xcat.mobile.coordinator.ComponentInfoImpl;
import xcat.mobile.coordinator.AppCoordinatorImpl;
import xcat.exceptions.NonstandardException;

import soaprmi.util.logging.Logger;

import com.mysql.jdbc.Driver;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

/**
 * A client for the AppCoordinator that interprets arguments passed by the script
 * and makes appropriate calls to the AppCoordinator
 *
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 * @version $Revision: 1.23 $  $Author: srikrish $ $Date: 2004/09/09 06:55:33 $ (GMT)
 */
public class AppCoordinatorClient {

  private static Logger logger = Logger.getLogger();

  // instance of the AppCoordinator that the client talks to
  private AppCoordinator appCoordinator;

  // the GSH for the HandleResolver
  private String appCoordinatorGSH;

  // the name of this application
  private String applicationName;

  // the unique ID for this application
  private String applicationID;

  // database parameters
  String dburl;
  String dbuser;
  String dbpasswd;

  /**
   * Default constructor - to be used when loading from database
   * @param applicationName_ the name of this application (coordinator)
   * @param applicationID_ the unique ID for this application
   */
  public AppCoordinatorClient(String applicationName_,
			      String applicationID_) 
    throws Exception {
    this(applicationName_, applicationID_, null, null, null);
  }

  /**
   * Constructor
   * @param applicationName_ the name of this application (coordinator)
   * @param applicationID_ the unique ID for this application
   * @param dburl_ the URL for the database to store the state
   * @param dbuser_ the username to use while connecting to the database
   * @param dbpasswd_ the password for the above user
   */
  public AppCoordinatorClient(String applicationName_,
			      String applicationID_,
			      String dburl_,
			      String dbuser_,
			      String dbpasswd_) 
    throws Exception {
    applicationName = applicationName_;
    applicationID = applicationID_;
    dburl = dburl_;
    dbuser = dbuser_;
    dbpasswd = dbpasswd_;

    // create an instance of the AppCoordinator
    appCoordinator = new AppCoordinatorImpl(applicationName,
					    applicationID,
					    dburl_,
					    dbuser_,
					    dbpasswd_);

    // create a GSH and add a reference into the HandleResolver
    appCoordinatorGSH = HandleResolver.createGSH("appCoordinator");
    appCoordinator.setGSH(appCoordinatorGSH);
    HandleResolver.addReference(appCoordinatorGSH, appCoordinator);

    // load the MySQL database driver
    new Driver();
  }

  /**
   * Returns the unique ID associated with this application
   */
  public String getApplicationID() {
    return applicationID;
  }

  /**
   * Creates a connection graph from an array of ComponentWrappers
   * @param componentWrappers the list of components that are part of the system
   */
  public void createConnectionGraph(ComponentWrapper[] componentWrappers)
    throws gov.cca.CCAException {
    
    // convert the array of ComponentWrappers to ComponentInfo
    ComponentInfo[] componentInfos = new ComponentInfo[componentWrappers.length];
    for (int i = 0; i < componentWrappers.length; i++) {
      // get all important data members from the componentWrapper
      String instanceName = componentWrappers[i].getInstanceName();
      String instanceHandle = 
	((XCATComponentID) componentWrappers[i].getComponentID()).getGSH();
      String instanceLocation = componentWrappers[i].getMachineName();
      String creationProto = componentWrappers[i].getCreationProto();
      String componentXML = componentWrappers[i].getComponentXML();
      componentInfos[i] = new ComponentInfoImpl(instanceName,
						instanceHandle,
						instanceLocation,
						creationProto,
						componentXML);
    }

    appCoordinator.createConnectionGraph(componentInfos);
  }

  /**
   * A request to migrate a component, which might be triggered by several reasons
   * @param componentWrapper the componentWrapper for the component requesting migration
   * @param targetLocation the location that the component should migrate to
   * @param masterStorageServiceURL the URL of the master storage service
   */
  public void migrateComponent(ComponentWrapper componentWrapper,
			       String targetLocation,
			       String masterStorageServiceURL)
    throws gov.cca.CCAException {
    appCoordinator.migrateComponent(componentWrapper.getInstanceName(),
				    targetLocation,
				    masterStorageServiceURL);
  }

  /**
   * A request to the AppCoordinator to checkpoint all components
   * @param masterStorageServiceURL the URL of the Master Storage Service
   */
  public void checkpointComponents(String masterStorageServiceURL)
    throws gov.cca.CCAException {
    appCoordinator.checkpointComponents(masterStorageServiceURL);
  }

  /**
   * A request to the AppCoordinator to restart from latest checkpoint
   */
  public void restartFromCheckpoint()
    throws gov.cca.CCAException {
    appCoordinator.restartFromCheckpoint();
  }

  /**
   * Loads the state of the Application Coordinator from a database
   */
  public void loadFromDatabase() 
    throws gov.cca.CCAException {
    appCoordinator.loadFromDatabase();
  }
}
