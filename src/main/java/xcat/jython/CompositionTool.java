/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.19 $ $Author: srikrish $ $Date: 2004/09/07 00:06:02 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * Copyright (c) 2002 Extreme! Lab, Indiana University. All rights reserved.
 *
 * This software is open source. See the bottom of this file for the licence.
 *
 * $Id: CompositionTool.java,v 1.19 2004/09/07 00:06:02 srikrish Exp $
 * @version $Revision: 1.19 $
 * http://www.extreme.indiana.edu/xcat
 */

package xcat.jython;

import soaprmi.util.logging.Logger;
import soaprmi.soaprpc.SoapServices;

import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import gov.cca.TypeMap;
import gov.cca.Port;
import gov.cca.Services;
import gov.cca.ComponentID;
import gov.cca.ConnectionID;
import gov.cca.CCAException;

import intf.ports.XCATPort;
import intf.ccacore.XCATComponentID;
import intf.services.XCATBuilderService;

import xcat.ccacore.XCATServicesImpl;
import xcat.services.XCATBuilderServiceImpl;
import xcat.exceptions.UnexpectedException;
import xcat.util.HandleResolver;

/**
 * @version $Revision: 1.19 $  $Author: srikrish $ $Date: 2004/09/07 00:06:02 $ (GMT)
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/**
 * The CompositionTool acts as a bridge between the XCAT JPython API
 * and the Java XCAT Services
 */
public class CompositionTool {

  private static Logger logger = Logger.getLogger();
  
  private Services core;
  private ComponentID compositionToolID;
  private XCATBuilderService builderService;

  /**
   * Default constructor
   */
  public CompositionTool() throws CCAException {

    logger.finest("called");

    // set the HandleResolver URL if it has been passed as a property
    String handleResolverURL = System.getProperty("HANDLE_RESOLVER_URL");
    if (handleResolverURL != null) 
      HandleResolver.setHandleResolverURL(handleResolverURL);

    // create an instance of the Builder Service
    try {
      logger.fine("Creating an instance of the Builder as a Grid service");
      // if the HandleResolver URL has not been set, use the Builder as
      // an in-memory HandleResolver
      builderService = 
	new XCATBuilderServiceImpl((HandleResolver.getHandleResolverURL() == null));
      String builderGSH = HandleResolver.createGSH("builderService");
      builderService.setGSH(builderGSH);
      HandleResolver.addReference(builderGSH, builderService);
    } catch (Exception e) {
      logger.severe("Can't instantiate Builder Service", e);
      throw new UnexpectedException("Can't instantiate Builder Service", e);
    }

    // create a new XCAT Services Object
    logger.fine("Creating a new Services object");
    core = new XCATServicesImpl("compositionTool");
    
    // get a component ID for the CompositionTool object
    compositionToolID = core.getComponentID();

    // set the GSH for this component
    String cidGSH = HandleResolver.createGSH("compositionTool");
    ((XCATPort) compositionToolID).setGSH(cidGSH);

    // add a reference for the componentID in the Handle Resolver
    HandleResolver.addReference(cidGSH, (XCATPort) compositionToolID);
  }

  /**
   * This method takes as an argument the pathname for the
   * component XML and creates a ComponentWrapper object
   * for the component
   * @param instanceName Name of the Component Instance
   * @param componentXML XML description of the Component
   */
  public ComponentWrapper createComponentWrapper(String instanceName, 
						 String componentXML) 
    throws CCAException {
    logger.finest("called with parameters: " +
		  " instanceName = " + instanceName);
    return new ComponentWrapper(instanceName, componentXML, builderService);
  }

  /**
   * This method takes as an argument the EnvObj for the
   * component XML and creates a ComponentWrapper object
   * for the component
   * @param instanceName Name of the Component Instance
   * @param componentEnv The TypeMap with the execution environment
   */
  public ComponentWrapper createComponentWrapper(String instanceName, 
						 TypeMap componentEnv) {
    logger.finest("called with parameters: " +
		  " instanceName = " + instanceName +
		  " componentEnv = " + componentEnv);
    return new ComponentWrapper(instanceName, componentEnv, builderService);
  }

  /**
   * This method connects a Uses Port to a Provides Port
   * @param usesPortComponent Component Wrapper of the Component whose Uses Port
   *                           is to be connected
   * @param usesPortName The name of the Uses Port
   * @param providesPortComponent Component Wrapper of the Component whose
   *                               Provides Port is to be connected
   * @param providesPortName The name of the Provides Port
   *
   * @return ConnectionID if the connect succeeds
   */
  public ConnectionID connect(ComponentWrapper usesPortComponent,
			      String usesPortName,
			      ComponentWrapper providesPortComponent,
			      String providesPortName)
    throws CCAException {
    logger.finest("called for uses port " + usesPortName +
		  " of component " + usesPortComponent.getComponentID().getInstanceName() +
		  " and provides port " + providesPortName +
		  " of component " + providesPortComponent.getComponentID().getInstanceName()
		  );
    return builderService.connect(usesPortComponent.getComponentID(),
				  usesPortName,
				  providesPortComponent.getComponentID(),
				  providesPortName);
  }

  /**
   * This method connects a WS port to a Web service
   * @param wsPortComponent Component Wrapper of the Component whose WS Port
   *                           is to be connected
   * @param wsPortName The name of the WS Port
   * @param endPointLocation The location of the Web service to connect to
   */
  public void connectToWS(ComponentWrapper wsPortComponent,
			  String wsPortName,
			  String endPointLocation)
    throws CCAException {
    logger.finest("called for uses port " + wsPortName +
		  " of component " + wsPortComponent.getComponentID().getInstanceName());
    builderService.connectToWS((XCATComponentID) wsPortComponent.getComponentID(),
			       wsPortName,
			       endPointLocation);
  }

  /**
   * This method disconnects a Uses Port from a Provides Port
   * @param connectionID The ID for the connection to be removed
   * @param timeout The timeout used by the xcat disconnect
   */
  public void disconnect(ConnectionID connectionID,
                         float timeout)
    throws CCAException {
    logger.finest("called for disconnection between uses port of " +
		  connectionID.getUser().getInstanceName() +
		  " and provides port of " +
		  connectionID.getProvider().getInstanceName());
    builderService.disconnect(connectionID,
			      timeout);
  }

  /**
   * This method disconnects a WS Port to a Web service
   * @param wsPortComponent Component Wrapper of the Component whose WS Port
   *                           is to be connected
   * @param wsPortName The name of the WS Port
   */
  public void disconnectFromWS(ComponentWrapper wsPortComponent,
			       String wsPortName)
    throws CCAException {
    logger.finest("called for disconnection between WS port " + wsPortName + 
		  " of " + wsPortComponent.getComponentID().getInstanceName());
    builderService.disconnectFromWS((XCATComponentID) wsPortComponent.getComponentID(),
				    wsPortName);
  }

  /**
   * Given a handle to a componentWrapper, this method creates a live instance
   * @param componentWrapper Component Wrapper of the Component to be instantiated
   */
  public void instantiate(ComponentWrapper componentWrapper)
    throws CCAException {
    logger.finest("called for component: " +
		  componentWrapper.getInstanceName());
    componentWrapper.createInstance();
  }

  /**
   * This method creates a live instance using the given timeout
   * @param componentWrapper Component Wrapper of the Component to be instantiated
   * @param timeout Timeout in milliseconds to be used for instantiation
   */
  public void instantiate(ComponentWrapper componentWrapper,
                          int timeout)
    throws CCAException {
    logger.finest("called for component: " +
		  componentWrapper.getInstanceName() +
		  " with timeout " + timeout + 
		  " ms");
    componentWrapper.createInstance(timeout);
  }

  /**
   * This method accepts a handle to a componentWrapper, the fully qualified
   * class name for the uses port, its type (which is a uri), the name by which
   * the provides port is registered, the method name, and the list of arguments,
   * and executes the method. It makes a best effort attempt to infer the types
   * of arguments from the methodParams array; primitive types and types of null 
   * parameters can not be inferred from it.
   * @param componentWrapper Component Wrapper of the target Component
   * @param portClassName Class name for the port interface
   * @param portType Type name for the port
   * @param providesPortName Name by which the provides port is registered
   * @param methodName Name of the method to be invoked
   * @param methodParams Array of objects containing parameters to the method
   */
  public Object executeMethod(ComponentWrapper componentWrapper,
                              String portClassName,
                              String portType,
                              String providesPortName,
                              String methodName,
                              Object[] methodParams)
    throws CCAException {
    
    // infer the types of the parameters
    Class[] methodParamTypes = new Class[methodParams.length];
    for (int i = 0; i < methodParamTypes.length; i++) {
      if (methodParams[i] == null) {
	logger.severe("Can't infer class name of null parameter " + i);
	throw new UnexpectedException("Can't infer class name of null parameter " +
				      i);
      }
      methodParamTypes[i] = methodParams[i].getClass();
    }

    // invoke the executeMethod with an extra parameter
    return executeMethod(componentWrapper,
			 portClassName,
			 portType,
			 providesPortName,
			 methodName,
			 methodParams,
			 methodParamTypes);
  }

  /**
   * This method accepts a handle to a componentWrapper, the fully qualified
   * class name for the uses port, its type (which is a uri), the name by which
   * the provides port is registered, the method name, and the list of arguments,
   * and executes the method.
   * @param componentWrapper Component Wrapper of the target Component
   * @param portClassName Class name for the port interface
   * @param portType Type name for the port
   * @param providesPortName Name by which the provides port is registered
   * @param methodName Name of the method to be invoked
   * @param methodParams Array of objects containing parameters to the method
   * @param methodParamTypes Array of types for the parameters
   */
  public Object executeMethod(ComponentWrapper componentWrapper,
                              String portClassName,
                              String portType,
                              String providesPortName,
                              String methodName,
                              Object[] methodParams,
			      Class[] methodParamTypes)
    throws CCAException {
    logger.finest("called");
    logger.finest(" Component Name = " + componentWrapper.getInstanceName() +
		  " Port Class = " + portClassName + 
		  " Port Type = " + portType +
		  " Provides Port Name = " + providesPortName);

    // register a uses port of the same type
    String usesPortName = "uses" + providesPortName;
    TypeMap tMap = core.createTypeMap();
    tMap.putString("portClass", portClassName);
    core.registerUsesPort(usesPortName, portType, tMap);

    // connect this uses port to the remote provides port
    ConnectionID connectionID = builderService.connect(compositionToolID,
						       usesPortName,
						       componentWrapper.getComponentID(),
						       providesPortName);
    // create a Class object for the port
    Class portClass = null;
    try {
      portClass = Class.forName(portClassName);
    } catch (ClassNotFoundException cnfe) {
      logger.severe("Can't find class: " + portClassName);
      throw new UnexpectedException("Can't find class: " + portClassName);
    }

    // get the method to invoke
    Method method = null;
    try {
      method = portClass.getMethod(methodName, methodParamTypes);
    } catch (NoSuchMethodException nsme) {
      logger.severe("No such method : " + methodName, 
		    nsme);
      throw new UnexpectedException("No such method : " +
				    methodName);
    }

    // make sure that the received port has the same class as expected
    Port port = core.getPort(usesPortName);
    if (!(portClass.isAssignableFrom(port.getClass()))) {
      throw new UnexpectedException("Received port not of the type: " + 
				    portClassName);
    } 

    // invoke the method
    Object retVal = null;
    try {
      retVal = method.invoke(port, methodParams);
    } catch (InvocationTargetException ite) {
      Throwable t = ite.getTargetException();
      logger.severe("Exception caught while executing method: " + methodName,
		    t);
      throw new UnexpectedException("Exception caught while executing method : " +
				    methodName + ", " + t.getLocalizedMessage(), 
				    t);
    } catch (Exception e) {
      logger.severe("Exception caught while executing method : " + methodName,
		    e);
      throw new UnexpectedException("Exception caught while executing method : " +
				    methodName + ", " + e.getMessage(), 
				    e);
    }

    // clean up
    core.releasePort(usesPortName);
    builderService.disconnect(connectionID, 0f);
    core.unregisterUsesPort(usesPortName);

    // return the received value
    return retVal;
  }

  /**
   * Returns an instance of gov.cca.TypeMap
   */
  public TypeMap createTypeMap() throws CCAException {
    logger.finest("called");
    return core.createTypeMap();
  }

  /**
   * Destroys this component instance
   * @param componentWrapper Component Wrapper of the Component to be killed
   */
  public void destroy(ComponentWrapper componentWrapper) 
    throws CCAException {
    logger.finest("called for Component : " + componentWrapper.getInstanceName());
    builderService.destroyInstance(componentWrapper.getComponentID(), 0f);
  }
}
