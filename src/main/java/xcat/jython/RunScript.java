/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.4 $ $Author: srikrish $ $Date: 2004/02/13 02:28:08 $ (GMT)
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.jython;

import org.python.util.PythonInterpreter;

import java.util.Properties;
import java.io.IOException;

import soaprmi.util.logging.Level;
import soaprmi.util.logging.Logger;

public class RunScript {

  private static Logger logger = Logger.getLogger();

  public static void main (String[] args) throws IOException {
logger.setLevel(Level.FINEST);
    /*
     * The user should give the scriptname as the first argument
     */
    if (args.length == 0) {
      System.out.println("Usage: java -classpath <CP> xcat.jython.RunScript <script.py>");
      System.exit(-1);
    }

    String pythonPath = System.getProperty("PYTHONPATH");
    String pathSeparator = System.getProperty("path.separator");

    Properties props = new Properties();
    final String PYTHONPATH = "python.path";
    props.setProperty(PYTHONPATH, "." + pathSeparator + pythonPath);

    logger.fine("Executing jython script : " + args[0] + " with " +
		PYTHONPATH + "=" + props.getProperty(PYTHONPATH));

    // remove script name from list of arguments
    String [] newargs = new String[args.length-1];
    for (int i = 1; i < args.length; i++) newargs[i-1] = args[i];

    // magic to execute python script through Jython
    PythonInterpreter.initialize(System.getProperties(), props, newargs);
    PythonInterpreter interp = new PythonInterpreter();
    interp.execfile (args[0]);

    /* done */
    System.out.println("Hit CTRL-C to exit");
    return;
  }
}
