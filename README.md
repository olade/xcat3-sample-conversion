# XCAT3 - Temperature Conversion Sample

This is a more standard Java project setup for the sample Conversion app of the
[XCAT3 framework](http://www.extreme.indiana.edu/xcat/tutorial/xcat3.html). It includes a JAR for the
[XCAT3 implementation](https://bitbucket.org/piemaster/xcat3-impl), but could still be refactored further.

To run as a Java project:

    Working dir: ${project_loc}/src/main/jython/com/computome/samples/xcat/conversion
    -classpath: (whatever it is in Eclipse)
    java xcat.jython.RunScript \
         ${project_loc}/src/main/jython/com/computome/samples/xcat/conversion/conversion.py \
         -DXCAT_HOME=C:\temp\xcat_3.0.6-alpha \
         -Dlog=com.computome.samples:FINEST
       
## Notes

* The second argument (after the main class) is the Jython script to execute.
* The `XCAT_HOME` property should probably be read from the system environment, I may just need to reboot.
* The log configuration is needed to see the debug output.

The program can also be executed by running the `conversion.py` script as a Jython run configuration. In that case, the
`PYTHONPATH` must contain the `/bin` directory of the project, plus most of the JARs in `/lib`. I gave up trying to find
the minimal set of dependencies, so I just included them all.